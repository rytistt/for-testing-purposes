| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  value 1 <br /> value 2 <br /> value 3  | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |

[I'm an inline-style link](https://www.evil.com)
this is new
<a href="https://www.evil.com">link2</a>
| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  value 1 <br /> value 2 <br /> value 3  | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |

[I'm an inline-style link](https://www.evil.com)

![Image of Yaktocat](./third.jpeg)

test/third.jpeg - should be transformed!
[a relative link](test/third.jpeg)
